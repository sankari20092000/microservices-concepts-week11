package com.hcl.department.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter

public class DepartmentDTO {  // plain old java obj POJO
	
	
	private  long departmentId;
	private  String departmentName;
	private  String  departmentAddress;
	private  String departmentCode;
	

}
