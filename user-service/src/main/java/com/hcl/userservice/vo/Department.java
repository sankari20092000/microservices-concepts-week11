package com.hcl.userservice.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Department {
	
	
	
	private  long departmentId;
	private  String departmentName;
	private  String  departmentAddress;
	private  String departmentCode;
	

}
