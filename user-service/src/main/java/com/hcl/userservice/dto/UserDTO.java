package com.hcl.userservice.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UserDTO { // pojo
	
	private long userId;
	private String username;
	private String email;
	private  long departmentId;

}
