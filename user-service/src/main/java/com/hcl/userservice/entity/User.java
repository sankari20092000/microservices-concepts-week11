package com.hcl.userservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data

@Entity
@Table(name="user_table")
public class User {
	
	@Id
	@GeneratedValue
	private long userId;
	private String username;
	private String email;
	private  long departmentId;  // foreign key
	

}
